package BasicItems;

import java.util.Random;

public class D6 implements Dice
{
	public int roll(){
		int[] sides = {1,2,3,4,5,6};
		int rand = new Random().nextInt(sides.length);
		
		System.out.println("You rolled a "+ sides[rand]);
		return sides[rand];
	}
}
