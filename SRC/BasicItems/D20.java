package BasicItems;

import java.util.Random;

public class D20 implements Dice
{
	public int roll(){
		int[] sides = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
		int rand = new Random().nextInt(sides.length);
		
		System.out.println("You rolled a "+ sides[rand]);
		return sides[rand];
	}
}
